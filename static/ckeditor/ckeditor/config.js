CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'en';
	// config.uiColor = '#AADC6E';
    config.allowedContent = {
    $1: {
            // Use the ability to specify elements as an object.
            elements: CKEDITOR.dtd,
            attributes: true,
            styles: true,
            classes: true
        }
    };
    config.disallowedContent = 'script; *[on*]';
	config.font_names = 'GoogleWebFonts;' + config.font_names;
	config.extraPlugins = 'youtube';
	config.youtube_responsive = true;
	config.youtube_older = false;
	config.youtube_privacy = false;
	config.youtube_controls = true;
};
