# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from HTMLParser import HTMLParser
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404, redirect

from machine.models import Machine, Category


class Parser(HTMLParser):

    def __init__(self):
        HTMLParser.__init__(self)

    def read(self, data):
        self._lines = []
        self.reset()
        self.feed(data)
        return '. '.join(self._lines)

    def handle_data(self, d):
        self._lines.append(d)


parser = Parser()


def products_for_menu():
    context = dict()
    context['prods1'] = Machine.objects.all()[0:3]
    context['prods2'] = Machine.objects.all()[3:5]
    context['prods3'] = Machine.objects.all()[5:8]
    context['prods4'] = Machine.objects.all()[8:10]
    return context

def index(request):
    featured = Machine.objects.filter(featured=True)
    for i in featured:
        if i.description is not None:
            desc = parser.read(i.description)
            if len(desc) > 100:
                desc = desc[:100] + '...'
            i.desc = desc[:-1]
        else:
            i.desc = ''
    context = dict()
    context['prods1'] = Machine.objects.all()[0:3]
    context['prods2'] = Machine.objects.all()[3:5]
    context['prods3'] = Machine.objects.all()[5:8]
    context['prods4'] = Machine.objects.all()[8:10]
    context['featured'] = featured
    return render(request,"home.html", context)

def about(request):
    return render(request, "about.html", products_for_menu())

def cr7(request):
    return render(request, "cr7.html", products_for_menu())
