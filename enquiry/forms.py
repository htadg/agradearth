from django import forms
from .models import Enquiry


class EnquiryForm(forms.ModelForm):
    class Meta:
        model = Enquiry
        fields = [
            "contact_name",
            "company_name",
            "address",
            "country",
            "email_id",
            "phone_number",
            "mobile_number",
            "subject",
            "details",
        ]
