# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Enquiry

class EnquiryAdmin(admin.ModelAdmin):
    list_display = ('contact_name','subject','mobile_number','details')




admin.site.register(Enquiry, EnquiryAdmin)
# Register your models here.
