# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Enquiry(models.Model):
    contact_name = models.CharField(max_length=120)
    company_name = models.CharField(max_length=120)
    address = models.TextField(null = True)
    country = models.CharField(max_length=120)
    email_id = models.EmailField()
    phone_number = models.CharField(max_length=15)
    mobile_number = models.CharField(max_length=15)
    subject = models.CharField(max_length=300)
    details = models.TextField()


    def __str__(self):
        return self.contact_name
    def __unicode__(self):
        return self.contact_name
