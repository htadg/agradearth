# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404, redirect
from .forms import EnquiryForm
from machine.models import Machine, Category
from django.contrib import messages

# Create your views here.


def products_for_menu():
    context = dict()
    context['prods1'] = Machine.objects.all()[0:3]
    context['prods2'] = Machine.objects.all()[3:5]
    context['prods3'] = Machine.objects.all()[5:8]
    context['prods4'] = Machine.objects.all()[8:10]
    return context

def enquiry(request):
    form = EnquiryForm(request.POST or None, request.FILES or None)
    if form.is_valid():
		instance = form.save(commit=False)
		instance.user = request.user
		instance.save()
		# message success
		messages.success(request, "Thank You! Your messages successfully sent!")
		return HttpResponseRedirect(request,"contact.html")
    context = products_for_menu()
    context['form']= form
    return render(request, "contact.html",context=context)
