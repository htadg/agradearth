# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Machine, Category

# Register your models here.

class MachineAdmin(admin.ModelAdmin):
    list_display = ('name', 'featured','category','description')
    prepopulated_fields = {"slug":("name","category",)}

# class CompanyAdmin(admin.ModelAdmin):
#     def get_model_perms(self, request):
#         return {}

class CategoryAdmin(admin.ModelAdmin):
    def get_model_perms(self, request):
        return {}


admin.site.register(Machine, MachineAdmin)
# admin.site.register(Company, CompanyAdmin)
admin.site.register(Category, CategoryAdmin)
