from django.conf.urls import url

from .views import machine, machinedetail

urlpatterns = [
url(r'^detail/(?P<slug>[\w-]+)/$', machinedetail, name="details"),
url('', machine),

]
