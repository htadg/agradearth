from django.db import models
from .utils import unique_slug_generator
from django.db.models.signals import pre_save
from ckeditor_uploader.fields import RichTextUploadingField
# Create your models here.


class Category(models.Model):
    category_name = models.CharField(max_length = 120)

    def __unicode__(self):
        return self.category_name
    def __str__(self):
        return self.category_name

def upload_location(instance, filename):
    #filebase, extension = filename.split(".")
    #return "%s/%s.%s" %(instance.id, instance.id, extension)
    MachineModel = instance.__class__
    new_id = MachineModel.objects.order_by("id").last().id + 1
    """
    instance.__class__ gets the model Post. We must use this method because the model is defined below.
    Then create a queryset ordered by the "id"s of each object,
    Then we get the last object in the queryset with `.last()`
    Which will give us the most recently created Model instance
    We add 1 to it, so we get what should be the same id as the the post we are creating.
    """
    return "%s/%s" %(new_id, filename)

class Machine(models.Model):
    name = models.CharField(max_length = 200)
    category = models.ForeignKey(Category)
    featured = models.BooleanField(default=False)
    slug = models.SlugField(unique=True)
    description = RichTextUploadingField(blank=True, null=True)
    image = models.ImageField(upload_to='images/',
            null=True,
            blank=True,
            width_field="width_field",
            height_field="height_field")
    height_field = models.IntegerField(default=0)
    width_field = models.IntegerField(default=0)

    def __unicode__(self):
        return self.name
    def __str__(self):
        return self.name



def pre_save_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        # instance.slug = create_slug(instance)
        instance.slug = unique_slug_generator(instance)

pre_save.connect(pre_save_post_receiver, sender=Machine)
