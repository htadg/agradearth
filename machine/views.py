# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from HTMLParser import HTMLParser
from .models import Machine, Category
from django.shortcuts import render, get_object_or_404

# Create your views here.


class Parser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)

    def read(self, data):
        self._lines = []
        self.reset()
        self.feed(data)
        return '. '.join(self._lines)

    def handle_data(self, d):
        self._lines.append(d)


parser = Parser()


def products_for_menu():
    context = dict()
    context['prods1'] = Machine.objects.all()[0:3]
    context['prods2'] = Machine.objects.all()[3:5]
    context['prods3'] = Machine.objects.all()[5:8]
    context['prods4'] = Machine.objects.all()[8:10]
    return context

def machine(request):
    queryset = Machine.objects.all()
    for i in queryset:
        desc = parser.read(i.description)
        if len(desc) > 100:
            desc = desc[:100] + '...'
        i.desc = desc[:-1]
    context = products_for_menu()
    context['queryset'] = queryset
    return render(request,"machines.html",context)

def machinedetail(request, slug=None):
    queryset = Machine.objects.all()
    mach = get_object_or_404(Machine,slug = slug)
    context = products_for_menu()
    context['queryset'] = queryset
    context['object'] = mach
    return render(request, "machine-detail.html", context)
